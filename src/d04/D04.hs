-- |
module D04 where

import Control.Monad
import D04.Input
import Data.Functor.Identity
import Data.Text.IO (readFile)
import Optics
import Paths_aoc
import Text.Trifecta (natural, parseFromFile)
import Prelude hiding (readFile)

withDef :: (Maybe (Bingo Int) -> Maybe Int) -> Maybe FilePath -> IO ()
withDef solve = print <=< fmap solve . parseFromFile (bingoP (natural <&> fromIntegral)) <=< maybe (getDataFileName "src/d04/assets/input.txt") pure
