-- |
module D04.Part2 where

import Control.Arrow (Arrow ((&&&)))
import Control.Monad ((<=<))
import D04.Common
import D04.Input
import Data.Maybe (fromMaybe)
import Optics
import Text.Trifecta hiding (Step)

solve :: Maybe (Bingo Int) -> Maybe Int
solve = (>>= ((walk <=< steps sweep) . (mapped %~ Just)))
  where
    walk :: [Step] -> Maybe Int
    walk (foldlOf' folded computeScore Nothing -> sc@(Just _)) = sc
    walk _ = Nothing

    computeScore :: Maybe Int -> Step -> Maybe Int
    computeScore scM@(preview _Just -> Just _) _ = scM
    computeScore _ (Step n (view boards -> [b@(completed -> True)])) = Just $ score b n
    computeScore _ _ = Nothing

sweep :: Step -> Step
sweep = bingo % boards %~ toListOf (folded % filtered (not . completed))
