-- |
module D04.Part1 where

import Control.Arrow (Arrow ((&&&)))
import Control.Monad (join)
import Control.Monad.Trans.State (State, evalState)
import D04.Common
import D04.Input
import Data.Maybe (fromMaybe)
import Optics
import Text.Trifecta hiding (Step)

solve :: Maybe (Bingo Int) -> Maybe Int
solve = (>>= walk . steps id . (mapped %~ Just))
  where
    walk :: Maybe [Step] -> Maybe Int
    walk (preview _Just -> Just (foldlOf' folded computeScore Nothing -> r@(Just _))) = r
    walk _ = Nothing

    computeScore :: Maybe Int -> Step -> Maybe Int
    computeScore scM@(preview _Just -> Just _) _ = scM
    computeScore _ (win -> Just (n, b)) = Just $ score b n
    computeScore _ _ = Nothing
