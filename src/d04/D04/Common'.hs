{-# LANGUAGE TupleSections #-}

module D04.Common' where

import D04.Input
import Data.Maybe (fromMaybe)
import Optics

data Step' = Step' {_number :: Maybe Int, _bingo :: Bingo (Maybe Int)}
  deriving (Show)

makeLenses ''Step'

-- steps :: (Step' -> Step') -> Bingo (Maybe Int) -> Maybe [Step']
-- steps sweepFn bgo = bgo ^. numbers & sequenceA . drop 1 . scanl go Nothing
--   where
--     go :: Maybe Step' -> Int -> Maybe Step'
--     go (preview (_Just % to sweepFn) -> Just stp) n = stp & number .~ n & bingo %~ ((numbers %~ tail) . mark n) & pure
--     go _ n = Step' n (bgo & numbers %~ tail & mark n) & pure

mark' :: Int -> Bingo (Maybe Int) -> Bingo (Maybe Int)
mark' n = mapped %~ \case (Just ((== n) -> True)) -> Nothing; x -> x

score' :: Board (Maybe Int) -> Int -> Int
score' (sumOf folded . (<&> fromMaybe 0) -> sc) = (sc *)

win' :: Step' -> Maybe (Int, Board (Maybe Int))
win' s@(findOf (bingo % boards % folded) completed' -> Just b) = (s ^. number) <&> (,b)
win' _ = Nothing

completed' :: Board (Maybe Int) -> Bool
completed' (Board b) = horiz b || (horiz . sequenceA) b
  where
    horiz :: Five (Five (Maybe Int)) -> Bool
    horiz = anyOf folded (allOf folded \case (preview _Nothing -> Just ()) -> True; _ -> False)
