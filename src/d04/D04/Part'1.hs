-- |
module D04.Part'1 where

import Control.Arrow (Arrow ((&&&)))
import Control.Monad (join)
import Control.Monad.Trans.Class (MonadTrans (lift))
import Control.Monad.Trans.State.Strict
import D04.Common'
import D04.Input
import Data.Maybe (fromMaybe)
import Optics
import Optics.State
import Optics.State.Operators

solve' :: Maybe (Bingo Int) -> Maybe Int
solve' = (>>= walk . (mapped %~ Just))
  where
    walk :: Bingo (Maybe Int) -> Maybe Int
    walk bgo = bgo ^. numbers & foldlOf' folded (\m n -> m >>= maybe (computeScore n) (pure . pure)) (pure Nothing) & (`evalState` Step' Nothing bgo)

    computeScore :: Int -> State Step' (Maybe Int)
    computeScore n = use (equality' % to win') >>= maybe next \(l, b) -> pure $ pure $ score' b l
      where
        next :: State Step' (Maybe Int)
        next = do
          number ?= n
          zoom bingo do
            use equality' <&> mark' n
            numbers %= tail
          pure Nothing
