-- |
module D04.Input where

import Data.Text.Strict.Optics (packed, unpacked)
import GHC.Generics (Generic)
import Optics
import Paths_aoc (getDataFileName)
import Text.Trifecta

data Five a = Five a a a a a
  deriving (Eq, Show, Generic, Functor, Foldable, Traversable)

instance Applicative Five where
  pure a = Five a a a a a
  Five f1 f2 f3 f4 f5 <*> Five a1 a2 a3 a4 a5 = Five (f1 a1) (f2 a2) (f3 a3) (f4 a4) (f5 a5)

newtype Board a = Board (Five (Five a))
  deriving (Show, Generic, Functor, Foldable, Traversable)

instance Applicative Board where
  pure a = Board $ pure (pure a)
  Board (Five f1 f2 f3 f4 f5) <*> Board (Five a1 a2 a3 a4 a5) = Board (Five (f1 <*> a1) (f2 <*> a2) (f3 <*> a3) (f4 <*> a4) (f5 <*> a5))

data Bingo a = Bingo {_numbers :: [Int], _boards :: [Board a]}
  deriving (Show, Generic, Functor, Foldable, Traversable)

makeLenses ''Bingo

bingoP :: (TokenParsing p, Integral a) => p a -> p (Bingo a)
bingoP pa = Bingo <$> ((pa <&> fromIntegral) `sepBy` comma) <*> many (boardP pa)
  where
    fiveP :: (TokenParsing p) => p a -> p (Five a)
    fiveP pa = Five <$> pa <*> pa <*> pa <*> pa <*> pa

    boardP :: (TokenParsing p) => p a -> p (Board a)
    boardP pa = Board <$> fiveP (fiveP pa)
