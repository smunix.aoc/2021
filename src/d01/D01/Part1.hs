-- |
module D01.Part1 where

import D01.Input (Input)
import Optics

data Two a = Two a a

solve :: Input -> Int
solve as@(_ : bs) = zipWith Two as bs & lengthOf (folded % filtered \(Two a b) -> a < b)
solve _ = 0
