-- |
module D01.Part2 where

import D01.Input (Input)
import qualified D01.Part1 as Part1
import Optics

data Three a = Three a a a deriving (Foldable)

solve :: Input -> Int
solve as@(_ : bs@(_ : cs)) = zipWith3 (((.) . (.) . (.)) sum Three) as bs cs & Part1.solve
solve _ = 0
