module D01.Input where

import Data.Text
import Data.Text.Optics (unpacked)
import Optics
import Prelude hiding (lines)

type Input = [Int]

parse :: Text -> Input
parse = (mapped %~ view (unpacked % to read)) . lines
