module D01 where

import Control.Monad ((<=<))
import D01.Input
import Data.Text.IO
import Paths_aoc (getDataFileName)
import Prelude hiding (readFile)

withDef :: (Input -> Int) -> Maybe FilePath -> IO ()
withDef solve = print <=< fmap (solve . parse) . readFile <=< maybe (getDataFileName "src/d01/assets/input.txt") pure
