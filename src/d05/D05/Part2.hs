-- |
module D05.Part2 where

import D05.Input
import qualified Data.HashMap.Strict as HM
import Optics

solve :: Maybe [Line Int] -> Maybe Int
solve = (>>= (pure . lengthOf (folded % filtered (> 1)) . foldlOf' folded step Empty))
  where
    step :: HM.HashMap (XY Int) Int -> Line Int -> HM.HashMap (XY Int) Int
    step m l@((^.. points) -> xys) =
      foldrOf folded (\xy m -> m & at xy %~ maybe (pure 1) (pure . (+ 1))) m xys
        & at (l ^. end) %~ maybe (pure 1) (pure . (+ 1))
