-- |
module D05.Input where

import Control.Applicative (Applicative (liftA2))
import Data.Hashable
import Data.Text.Strict.Optics (packed, unpacked)
import GHC.Generics (Generic)
import Optics
import Paths_aoc (getDataFileName)
import Text.Trifecta

data XY a = XY {_x :: a, _y :: a}
  deriving (Show, Eq, Generic, Hashable, Functor, Foldable, Traversable)

makeLenses ''XY

instance Applicative XY where
  pure a = XY a a
  XY fx fy <*> XY ax ay = XY (fx ax) (fy ay)

data Line a = Line {_start :: XY a, _end :: XY a}
  deriving (Show, Eq, Generic, Functor, Foldable, Traversable)

makeLenses ''Line

instance Applicative Line where
  pure a = Line (pure a) (pure a)
  Line fs fe <*> Line as ae = Line (fs <*> as) (fe <*> ae)

lineP :: forall a p. (TokenParsing p) => p a -> p (Line a)
lineP pa = Line <$> (xyP <* token (string "->")) <*> xyP
  where
    xyP :: p (XY a)
    xyP = XY <$> (pa <* comma) <*> pa

points :: forall a. (Num a, Ord a) => Fold (Line a) (XY a)
points = unfolded walk
  where
    walk :: Line a -> Maybe (XY a, Line a)
    walk line
      | line ^. start /= line ^. end = Just (line ^. start, line & start %~ liftA2 (+) unit)
      | otherwise = Nothing
      where
        unit :: XY a
        unit = (-) <$> line ^. end <*> line ^. start <&> signum
