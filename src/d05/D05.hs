-- |
module D05 where

import Control.Monad
import D05.Input
import Data.Functor.Identity
import Data.Text.IO (readFile)
import Optics
import Paths_aoc
import Text.Trifecta (many, natural, parseFromFile)
import Prelude hiding (readFile)

withDef :: (Maybe [Line Int] -> Maybe Int) -> Maybe FilePath -> IO ()
withDef solve = print <=< fmap solve . parseFromFile (many $ lineP (natural <&> fromIntegral)) <=< maybe (getDataFileName "src/d05/assets/input.txt") pure
