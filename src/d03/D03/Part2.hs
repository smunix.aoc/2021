{-# LANGUAGE AllowAmbiguousTypes #-}

-- |
module D03.Part2 where

import Control.Arrow (Arrow ((&&&)))
import D03.Input
  ( Bit (..),
    Twelve,
    num,
    twelveToList,
  )
import D03.Part1 (rate)
import Data.Functor.Identity
import GHC.TypeLits (Symbol)
import Optics
import Y.Y (Coalgebra, Y (Y), ana)

data TreeF a where
  Leaf :: [] Bit -> TreeF a
  Node :: {_zs :: (Int, a), _os :: (Int, a)} -> TreeF a
  deriving (Functor, Show)

type Material a = (Y TreeF -> a) -> (Int, Y TreeF) -> (Int, Y TreeF) -> a

class HasMaterial (s :: Symbol) where
  material :: Material [Bit]

instance HasMaterial "O2" where
  material k z o =
    if z ^. _1 > o ^. _1
      then O : k (z ^. _2)
      else I : k (o ^. _2)

instance HasMaterial "CO2" where
  material k z o =
    if o ^. _1 < z ^. _1
      then I : k (o ^. _2)
      else O : k (z ^. _2)

rating :: forall m. (HasMaterial m) => Y TreeF -> Int
rating = view (to (scrubber @m) % re (twelveToList equality) % num)

scrubber :: forall m. (HasMaterial m) => Y TreeF -> [Bit]
scrubber (Y (Leaf bts)) = bts
scrubber (Y Node {..}) = material @m (scrubber @m) _zs _os

mkTree :: [Twelve Identity Bit] -> Y TreeF
mkTree = ana coa . (mapped %~ (^. twelveToList equality))

node :: [] [Bit] -> ([] [Bit], [] [Bit])
node = foldrOf folded nodeF (Empty, Empty)
  where
    nodeF :: [Bit] -> ([] [Bit], [] [Bit]) -> ([] [Bit], [] [Bit])
    nodeF [] = id
    nodeF (O : xs) = _1 %~ (xs :<)
    nodeF (I : xs) = _2 %~ (xs :<)

coa :: Coalgebra TreeF [[] Bit]
coa [] = Leaf mempty
coa [xs] = Leaf xs
coa xs@(node -> (zhs@(lengthOf folded -> zlen), ohs@(lengthOf folded -> olen))) =
  Node {_zs = (zlen, zhs), _os = (olen, ohs)}

solve :: [Twelve Identity Bit] -> Int
solve =
  uncurry (*)
    . (rating @"O2" &&& rating @"CO2")
    . mkTree

-- >>> parse $ "110001010110\n011101111101"
-- 507776
