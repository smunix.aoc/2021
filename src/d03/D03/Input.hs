-- |
module D03.Input where

import Data.Function (fix)
import Data.Functor.Identity
import Data.Monoid
import Data.Text
import Data.Text.Strict.Optics (packed, unpacked)
import GHC.Generics (Generic)
import Optics
import Prelude hiding (lines, words)

data Bit = O | I
  deriving (Enum, Show)

makePrisms ''Bit

instance Semigroup Bit where
  O <> x = x
  x <> _ = x

instance Monoid Bit where
  mempty = O

data Twelve f a = Twelve
  { _b01 :: f a,
    _b02 :: f a,
    _b03 :: f a,
    _b04 :: f a,
    _b05 :: f a,
    _b06 :: f a,
    _b07 :: f a,
    _b08 :: f a,
    _b09 :: f a,
    _b10 :: f a,
    _b11 :: f a,
    _b12 :: f a
  }
  deriving (Show, Foldable, Eq, Ord)

makeLenses ''Twelve

instance (Semigroup (f a)) => Semigroup (Twelve f a) where
  a <> b =
    Twelve
      { _b01 = a ^. b01 <> b ^. b01,
        _b02 = a ^. b02 <> b ^. b02,
        _b03 = a ^. b03 <> b ^. b03,
        _b04 = a ^. b04 <> b ^. b04,
        _b05 = a ^. b05 <> b ^. b05,
        _b06 = a ^. b06 <> b ^. b06,
        _b07 = a ^. b07 <> b ^. b07,
        _b08 = a ^. b08 <> b ^. b08,
        _b09 = a ^. b09 <> b ^. b09,
        _b10 = a ^. b10 <> b ^. b10,
        _b11 = a ^. b11 <> b ^. b11,
        _b12 = a ^. b12 <> b ^. b12
      }

instance (Monoid (f a)) => Monoid (Twelve f a) where
  mempty =
    Twelve
      { _b01 = mempty,
        _b02 = mempty,
        _b03 = mempty,
        _b04 = mempty,
        _b05 = mempty,
        _b06 = mempty,
        _b07 = mempty,
        _b08 = mempty,
        _b09 = mempty,
        _b10 = mempty,
        _b11 = mempty,
        _b12 = mempty
      }

data Count a = Count {_zeroes :: Sum a, _ones :: Sum a}
  deriving (Show)

makeLenses ''Count

instance (Num a) => Semigroup (Count a) where
  Count zas oas <> Count zbs obs = Count (zas <> zbs) (oas <> obs)

instance (Num a) => Monoid (Count a) where
  mempty = Count mempty mempty

-- >>> Count 1 2 <> Count 3 4
-- Count {_zeroes = Sum {getSum = 4}, _ones = Sum {getSum = 6}}

-- >>> foldMap Sum ([1..10 :: Int])
-- Sum {getSum = 55}

-- >>> foldMap Product ([1..10 :: Int])
-- Product {getProduct = 3628800}

-- 1 + 2 + 3 + 4 <=> O(n) <=> 1 + (2 + (3 + 4))
-- 1 + 2 + 3 + 4 <=> O(log(n)) <=> (1 + 2) + (3 + 4) + (5 + 6) + (7 + 8)

hoist :: (f a -> g b) -> Twelve f a -> Twelve g b
hoist h twelve =
  Twelve
    { _b01 = twelve ^. b01 % to h,
      _b02 = twelve ^. b02 % to h,
      _b03 = twelve ^. b03 % to h,
      _b04 = twelve ^. b04 % to h,
      _b05 = twelve ^. b05 % to h,
      _b06 = twelve ^. b06 % to h,
      _b07 = twelve ^. b07 % to h,
      _b08 = twelve ^. b08 % to h,
      _b09 = twelve ^. b09 % to h,
      _b10 = twelve ^. b10 % to h,
      _b11 = twelve ^. b11 % to h,
      _b12 = twelve ^. b12 % to h
    }

type Input f a = [Twelve f a]

parse :: Text -> Input Identity Bit
parse = (mapped %~ review text) . lines

twelveToList ::
  forall f g a b.
  ( Foldable f,
    Foldable g,
    Applicative g,
    Cons (f b) (f b) b b,
    AsEmpty (f b),
    Monoid a
  ) =>
  Iso' a b ->
  Iso' (Twelve g a) (f b)
twelveToList isoAB = iso from' to'
  where
    from' :: Twelve g a -> f b
    from' = foldrOf folded (\e -> ((e ^. isoAB) :<)) Empty

    to' :: f b -> Twelve g a
    to' = convert . toListOf folded
      where
        pad :: [b] -> [b]
        pad str@(lengthOf folded -> !len)
          | len < 12 = pad (mempty ^. isoAB :< str)
          | otherwise = str

        convert :: [b] -> Twelve g a
        convert (pad -> bs)
          | [b01, b02, b03, b04, b05, b06, b07, b08, b09, b10, b11, b12] <- bs =
            Twelve
              { _b01 = b2a b01,
                _b02 = b2a b02,
                _b03 = b2a b03,
                _b04 = b2a b04,
                _b05 = b2a b05,
                _b06 = b2a b06,
                _b07 = b2a b07,
                _b08 = b2a b08,
                _b09 = b2a b09,
                _b10 = b2a b10,
                _b11 = b2a b11,
                _b12 = b2a b12
              }
          | otherwise = error "input is longer than 12 bits"
          where
            b2a :: b -> g a
            b2a = pure . review isoAB

isoBitChar :: Iso' Bit Char
isoBitChar = iso (\case O -> '0'; I -> '1') (\case '0' -> O; '1' -> I; x -> error $ "char '" <> show x <> "' is not a bit information")

isoCharBit :: Iso' Char Bit
isoCharBit = re isoBitChar

-- >>> review (twelveToList isoBitChar) ("110001010110" :: [Char] ) :: Twelve Identity Bit
-- Twelve {_b01 = Identity I, _b02 = Identity I, _b03 = Identity O, _b04 = Identity O, _b05 = Identity O, _b06 = Identity I, _b07 = Identity O, _b08 = Identity I, _b09 = Identity O, _b10 = Identity I, _b11 = Identity I, _b12 = Identity O}

-- >>> review (twelveToList isoBitChar) ("1010110" :: [Char] ) :: Twelve Identity Bit
-- Twelve {_b01 = Identity O, _b02 = Identity O, _b03 = Identity O, _b04 = Identity O, _b05 = Identity O, _b06 = Identity I, _b07 = Identity O, _b08 = Identity I, _b09 = Identity O, _b10 = Identity I, _b11 = Identity I, _b12 = Identity O}

-- >>> view (twelveToList isoBitChar) (review (twelveToList isoBitChar) ("1010110" :: [Char] ) :: Twelve Identity Bit) :: [Char]
-- "000001010110"

text :: Iso' (Twelve Identity Bit) Text
text = iso (^. twelveToList @[] isoBitChar % packed) (^. unpacked % re (twelveToList isoBitChar))

string :: Iso' (Twelve Identity Bit) String
string = iso (^. twelveToList @[] isoBitChar) (^. re (twelveToList isoBitChar))

-- >>> parse "110001010110"
-- [Twelve {_b01 = Identity I, _b02 = Identity I, _b03 = Identity O, _b04 = Identity O, _b05 = Identity O, _b06 = Identity I, _b07 = Identity O, _b08 = Identity I, _b09 = Identity O, _b10 = Identity I, _b11 = Identity I, _b12 = Identity O}]

-- >>> review string ("110001010110" :: Text )
-- Twelve {_b01 = Identity I, _b02 = Identity I, _b03 = Identity O, _b04 = Identity O, _b05 = Identity O, _b06 = Identity I, _b07 = Identity O, _b08 = Identity I, _b09 = Identity O, _b10 = Identity I, _b11 = Identity I, _b12 = Identity O}

-- >>> view string $ review string ("10" :: Text)
-- "000000000010"

num :: Integral n => Iso' (Twelve Identity Bit) n
num =
  iso
    (fromIntegral . foldlOf' folded (\a e -> fromEnum e + a * 2) 0)
    (review string . toListOf (bits % to \case O -> '0'; I -> '1') . fromIntegral)
  where
    bits :: Fold Int Bit
    bits = unfolded genBit

    genBit :: Int -> Maybe (Bit, Int)
    genBit ((`quotRem` 2) -> (0, 0)) = Nothing
    genBit ((`quotRem` 2) -> (q, 0)) = Just (O, q)
    genBit ((`quotRem` 2) -> (q, 1)) = Just (I, q)
    genBit _ = Nothing

-- >>> (mapped %~ (view num)) . parse $ "111111111111"
-- [4095]

-- >>> (mapped %~ (view num)) . parse $ "110001010110\n011101111101"
-- [3158,1917]

-- >>> (review num) 9
-- Twelve {_b01 = Identity O, _b02 = Identity O, _b03 = Identity O, _b04 = Identity O, _b05 = Identity O, _b06 = Identity O, _b07 = Identity O, _b08 = Identity O, _b09 = Identity I, _b10 = Identity O, _b11 = Identity O, _b12 = Identity I}

-- >>> (view num . review num) 9
-- 9
