{-# LANGUAGE AllowAmbiguousTypes #-}

-- |
module D03.Part1 where

import Control.Arrow (Arrow ((&&&)))
import D03.Input
import Data.Functor.Identity
import Data.Text
import Data.Text.Optics
import GHC.TypeLits (Symbol)
import Optics

class HasBit (s :: Symbol) where
  bit :: Getter (Count Int) (Identity Bit)

instance HasBit "gamma" where
  bit = to \c ->
    if c ^. zeroes > c ^. ones
      then pure O
      else pure I

instance HasBit "epsilon" where
  bit = to \c ->
    if c ^. zeroes > c ^. ones
      then pure I
      else pure O

rate :: forall (s :: Symbol). HasBit s => Twelve Count Int -> Twelve Identity Bit
rate twelve = Twelve {..}
  where
    _b01 = twelve ^. b01 % bit @s
    _b02 = twelve ^. b02 % bit @s
    _b03 = twelve ^. b03 % bit @s
    _b04 = twelve ^. b04 % bit @s
    _b05 = twelve ^. b05 % bit @s
    _b06 = twelve ^. b06 % bit @s
    _b07 = twelve ^. b07 % bit @s
    _b08 = twelve ^. b08 % bit @s
    _b09 = twelve ^. b09 % bit @s
    _b10 = twelve ^. b10 % bit @s
    _b11 = twelve ^. b11 % bit @s
    _b12 = twelve ^. b12 % bit @s

solve :: Input Identity Bit -> Int
solve =
  uncurry (*)
    . (view num . rate @"gamma" &&& view num . rate @"epsilon")
    . foldMapOf
      folded
      ( hoist \case
          Identity O -> Count {_zeroes = 1, _ones = 0}
          Identity I -> Count {_zeroes = 0, _ones = 1}
      )

-- >>> solve $ parse $ "110001010110\n011101111101"
-- 507776
