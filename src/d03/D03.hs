-- |
module D03 where

import Control.Monad
import D03.Input
import Data.Functor.Identity
import Data.Text.IO (readFile)
import Paths_aoc
import Prelude hiding (readFile)

withDef :: (Input Identity Bit -> Int) -> Maybe FilePath -> IO ()
withDef solve = print <=< fmap (solve . parse) . readFile <=< maybe (getDataFileName "src/d03/assets/input.txt") pure
