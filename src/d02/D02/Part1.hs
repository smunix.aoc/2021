-- |
module D02.Part1 where

import Control.Arrow (Arrow ((&&&)))
import D02.Input (Dir (..), Input)
import Optics

data Pos a where
  Pos :: {_horiz :: a, _depth :: a} -> Pos a

makeLenses ''Pos

deriving instance (Show a) => Show (Pos a)

solve :: forall a. (Num a) => Input a -> a
solve = ((*) <$> _depth <*> _horiz) . foldlOf' folded step (Pos @a 0 0)
  where
    step :: Pos a -> Dir a -> Pos a
    step pos = \case
      Up x -> pos & depth %~ subtract x
      Dn x -> pos & depth %~ (+ x)
      Fwd x -> pos & horiz %~ (+ x)
