-- |
module D02.Input where

import Data.Text
import Data.Text.Strict.Optics (unpacked)
import Optics
import Prelude hiding (lines, words)

data Dir a = Up a | Dn a | Fwd a

makePrisms ''Dir

deriving instance (Show a) => Show (Dir a)

type Input a = [Dir a]

parse :: forall a. Read a => Text -> Input a
parse = (mapped %~ (mkDir . words)) . lines
  where
    mkDir :: [Text] -> Dir a
    mkDir ["forward", x] = Fwd (x ^. cast)
    mkDir ["up", x] = Up (x ^. cast)
    mkDir ["down", x] = Dn (x ^. cast)
    mkDir e = error (show e <> " is not supported")

    cast :: Getter Text a
    cast = unpacked % to read
