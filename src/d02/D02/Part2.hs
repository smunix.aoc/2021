-- |
module D02.Part2 where

import Control.Arrow (Arrow ((&&&)))
import D02.Input (Dir (..), Input)
import Optics

data Target a where
  Target :: {_horiz :: a, _depth :: a, _aim :: a} -> Target a

makeLenses ''Target

deriving instance (Show a) => Show (Target a)

solve :: forall a. (Num a) => Input a -> a
solve = ((*) <$> _depth <*> _horiz) . foldlOf' folded step (Target @a 0 0 0)
  where
    step :: Target a -> Dir a -> Target a
    step target = \case
      Up x -> target & aim %~ subtract x
      Dn x -> target & aim %~ (+ x)
      Fwd x ->
        target
          & horiz %~ (+ x)
          & depth %~ (+ (x * target ^. aim))
