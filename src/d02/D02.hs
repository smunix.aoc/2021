-- |
module D02 where

import Control.Monad ((<=<))
import D02.Input
import Data.Text.IO
import Paths_aoc (getDataFileName)
import Prelude hiding (readFile)

withDef :: ((Input Int -> Int) -> Maybe FilePath -> IO ())
withDef solve = print <=< fmap (solve . parse) . readFile <=< maybe (getDataFileName "src/d02/assets/input.txt") pure
