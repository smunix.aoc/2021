module Part where

import GHC.Generics
import Options.Generic

data Part = One | Two | Unknown String
  deriving (Show, Generic)

instance Read Part where
  readsPrec _ =
    \case
      "one" -> [(One, [])]
      "two" -> [(Two, [])]
      x -> [(Unknown x, [])]

instance ParseRecord Part

instance ParseField Part

instance ParseFields Part
