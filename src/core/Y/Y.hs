-- | Y combinator (for recursive types)
module Y.Y where

import Data.Function (fix)
import Optics

newtype Y f where
  Y :: {_y :: f (Y f)} -> Y f

makeLenses ''Y

type Algebra f a = f a -> a

type Coalgebra f a = a -> f a

cata :: (Functor f) => Algebra f a -> Y f -> a
cata alg = alg . (mapped %~ cata alg) . (^. y)

ana :: (Functor f) => Coalgebra f a -> a -> Y f
ana coa = Y . (mapped %~ ana coa) . coa

hylo :: (Functor f) => Coalgebra f a -> Algebra f b -> a -> b
hylo coa alg = alg . (mapped %~ hylo coa alg) . coa
