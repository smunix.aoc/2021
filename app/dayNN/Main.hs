module Main where

import qualified D01
import qualified D01.Part1
import qualified D01.Part2
import qualified D02
import qualified D02.Part1
import qualified D02.Part2
import qualified D03
import qualified D03.Part1
import qualified D03.Part2
import qualified D04
import qualified D04.Part1
import qualified D04.Part2
import qualified D05
import qualified D05.Part1
import qualified D05.Part2
import Optics
import qualified Options.Applicative as OA
import Options.Generic
import Part

newtype D = D {d :: Part -> Maybe FilePath -> IO ()} deriving (Generic)

makeLensesFor [("d", "dL")] ''D

instance ParseRecord D where
  parseRecord = parseField (Just "d") (Just "d") (Just 'd') (Just "03")

instance ParseField D where
  readField = OA.maybeReader \case
    "01" -> parts "01" D01.withDef D01.Part1.solve D01.Part2.solve
    "02" -> parts "02" D02.withDef D02.Part1.solve D02.Part2.solve
    "03" -> parts "03" D03.withDef D03.Part1.solve D03.Part2.solve
    "04" -> parts "04" D04.withDef D04.Part1.solve D04.Part2.solve
    "05" -> parts "05" D05.withDef D05.Part1.solve D05.Part2.solve
    x -> Just . D $ \_ _ -> print $ "unsupported day '" <> x <> "'"
    where
      parts :: forall t. String -> (t -> Maybe FilePath -> IO ()) -> t -> t -> Maybe D
      parts d def p1 p2 = Just . D $ \case
        p@One -> io p (def p1)
        p@Two -> io p (def p2)
        p@(Unknown x) -> io p (const $ print ())
        where
          io :: Part -> (Maybe FilePath -> IO ()) -> Maybe FilePath -> IO ()
          io p act inp = do putStr $ "day " <> d <> "-" <> show p <> ": "; act inp

instance ParseFields D

data Opts w = Opts
  { day :: w ::: [D] <?> "day",
    part :: w ::: [Part] <?> "part",
    input :: w ::: Maybe String <?> "input"
  }
  deriving (Generic)

makeLensesFor
  [ ("day", "dayL"),
    ("part", "partL"),
    ("input", "inputL")
  ]
  ''Opts

instance ParseRecord (Opts Wrapped) where
  parseRecord = parseRecordWithModifiers lispCaseModifiers {shortNameModifier = firstLetter}

main :: IO ()
main =
  unwrapRecord "AoC 2021"
    >>= \opts ->
      sequence_ $
        (opts ^.. dayL % folded % dL)
          <*> (opts ^. partL)
          <*> pure (opts ^. inputL)
