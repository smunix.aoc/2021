{
  description = "Advent of Code 2021";

  nixConfig = {
    substituters = [ "https://hydra.iohk.io" ];
    trusted-public-keys =
      [ "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ=" ];
    bash-prompt =
      "\\[\\033[1m\\][dev-aoc-2021]\\[\\033[m\\]\\040\\w\\040λ\\040";
  };

  inputs = {
    hn = { url = "github:input-output-hk/haskell.nix"; };
    fu.url = "github:numtide/flake-utils/master";
    np.follows = "hn/nixpkgs-unstable";
    hh = {
      url = "github:patrickt/haskell-hedgehog?ref=ghc-9.2";
      flake = false;
      # type = "github";
      # owner = "patrickt";
      # repo = "haskell-hedgehog";
      # ref = "ghc-9.2";
    };
  };

  outputs = { self, np, fu, hn, hh }:
    fu.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        overlay = self: _:
          with self; {
            aoc = haskell-nix.project' {
              src = ./.;
              compiler-nix-name = "ghc8107";
              shell = {
                tools = {
                  cabal = { };
                  haskell-language-server = "latest";
                };
                buildInputs = [ ];
              };
              modules = [{ dontStrip = false; }];
            };
          };
        overlays = [ hn.overlay overlay ];
        flake = with (import np {
          inherit system overlays;
          inherit (hn) config;
        });
          aoc.flake { };
      in flake // { defaultApp = flake.apps."aoc:exe:dayNN"; });
}
